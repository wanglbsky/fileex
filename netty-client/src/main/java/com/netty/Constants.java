package com.netty;

public class Constants {

    public static final String SERVER_URL="http://"+Constants.SERVER_IP+":8883/netty/";

    public static final String SERVER_IP="192.168.4.247";
//    public static final String SERVER_IP="127.0.0.1";

    public static final String WEBSOCKET_PORT="8787";

}
